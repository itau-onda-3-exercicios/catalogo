package br.com.itau.catalogo;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CursoService {
	
	@Autowired
	CursoRepository cursoRepository;
	
	public Curso cadastrar(Curso curso) {
		return cursoRepository.save(curso);
	}
	
	public Optional<Curso> buscar(String nome){
		return cursoRepository.findByNome(nome);
	}

}
