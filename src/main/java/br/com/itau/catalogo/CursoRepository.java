package br.com.itau.catalogo;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

public interface CursoRepository extends CrudRepository<Curso, String> {
	
	public Optional<Curso> findByNome(String nome);
}
