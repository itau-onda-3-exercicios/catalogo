package br.com.itau.catalogo;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping()
public class CursoController {
	@Autowired
	CursoService cursoService;
	
	@PostMapping("/inserir")
	public ResponseEntity criar(@RequestBody Curso curso){
		curso = cursoService.cadastrar(curso);
		
		return ResponseEntity.status(201).body(curso);
	}
	
	@GetMapping("/{nome}")
	public ResponseEntity buscar(@PathVariable String nome) {
		Optional<Curso> cursoOpcional = cursoService.buscar(nome);
		
		if(cursoOpcional.isPresent()) {
			return ResponseEntity.ok(cursoOpcional.get());
		}
		
		return ResponseEntity.notFound().build();
	}
}
